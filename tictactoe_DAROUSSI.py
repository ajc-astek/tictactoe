############## CONSTANTS ##############

NB_PLAYERS=2
GRID_SIZE=3
LINE_SIZE=3

############## FUNCTIONS ##############

def initPlayers():
  global NB_PLAYERS

  players=[]
  player_signs=('x','o','@','#','$','&','=','^','%','?')

  if(NB_PLAYERS > len(player_signs)):
    print("Number of players too high ({NB_PLAYERS})\nMax players = {len(player_signs)}\nNumber of players set to {len(player_signs)}\n")
    NB_PLAYERS = len(player_signs)

  for i in range(NB_PLAYERS):
    player={"nom":"Player"+str(i+1),"sign":player_signs[i]}
  
    name=input(f"Enter your name Player{i+1}: ")
    if name != "":
      player["nom"] = name

    players.append(player)
    print(f"Welcome {player['nom']} !!!\n")
  return players

def createEmptyGrid():
  grid=[]
  for i in range(GRID_SIZE):
    grid.append([])
    for j in range(GRID_SIZE):
      grid[i].append(0)
  return grid

def drawGrid(grid):
  # header
  string=" |" 
  for i in range(GRID_SIZE):
    string+=str(i)+"|"
  string+="\n"

  # lines
  for i in range(GRID_SIZE):
    string+=str(i)+"|"
    for j in range(GRID_SIZE):
      if(grid[i][j]==0):
        string+=" |"
      else:
        string+=grid[i][j]+"|"
    string+="\n"
  print(string)

def checkCoord(string):
  if string == "" :
    return False

  splited_coord = string.split(',')
  
  if len(splited_coord)==2 and splited_coord[0].isdigit() and splited_coord[1].isdigit():
    x = int(splited_coord[0])
    y = int(splited_coord[1])
    if 0<=x<GRID_SIZE and 0<=y<GRID_SIZE:
      return True
    else:
      print("Error: x or y is out of bound")
  else:
    print("Error: x or y isn't a number")

  return False

def getCoord(string): 
  splited_coord = string.split(',')
  x=int(splited_coord[0])
  y=int(splited_coord[1])
  return (x,y)

def updateGrid(grid,coord,sign):
  x=coord[0]
  y=coord[1]
  grid[y][x]=sign

def isCellOccupied(grid,coord):
  x=coord[0]
  y=coord[1]
  return (grid[y][x] == 0)

def isVerticalLineComplete(grid,coord,sign):
  count=1 
  x=coord[0]
  y=coord[1]

    
  for i in range(1,LINE_SIZE):
    if((y+i)<GRID_SIZE and grid[y+i][x]==sign):
      count+=1
    if((y-i)>=0 and grid[y-i][x]==sign):
      count+=1

  return (count == LINE_SIZE)

def isHorizontalLineComplete(grid,coord,sign):  
  count=1 
  x=coord[0]
  y=coord[1]
  
  for i in range(1,LINE_SIZE):
    if((x+i)<GRID_SIZE and grid[y][x+i]==sign):
      count+=1
    if((x-i)>=0 and grid[y][x-i]==sign):
      count+=1

  return (count == LINE_SIZE)

def isBottomRightTopLeftDiagonalLineComplete(grid,coord,sign):  
  count=1 
  x=coord[0]
  y=coord[1]
  
  for i in range(1,LINE_SIZE):
    if((x+i)<GRID_SIZE and (y+i)<GRID_SIZE and grid[y+i][x+i]==sign):
      count+=1
    if((x-i)>=0 and (y-i)>=0 and grid[y-i][x-i]==sign):
      count+=1

  return (count == LINE_SIZE)

def isBottomLefttTopRighDiagonalLineComplete(grid,coord,sign):  
  count=1 
  x=coord[0]
  y=coord[1]
  
  for i in range(1,LINE_SIZE):
    if((x-i)>=0 and (y+i)<GRID_SIZE and grid[y+i][x-i]==sign):
      count+=1
    if((x+i)<GRID_SIZE and (y-i)>=0 and grid[y-i][x+i]==sign):
      count+=1

  return (count == LINE_SIZE)

def isLineComplete(grid,coord,sign):
  return (isVerticalLineComplete(grid,coord,sign) 
    or isHorizontalLineComplete(grid,coord,sign) 
    or isBottomRightTopLeftDiagonalLineComplete(grid,coord,sign) 
    or isBottomLefttTopRighDiagonalLineComplete(grid,coord,sign))

def isGridFull(grid):
  res=True
  for i in range(GRID_SIZE):
    for j in range(GRID_SIZE):
      if grid[j][i] == 0:
        res=False
  return res

def checkGrid(grid,coord,sign):
  if(isLineComplete(grid,coord,sign)):
    print(f"\n{player['nom']} wins!")
    return True
  elif(isGridFull(grid)):
    print(f"Draw")
    return True
  return False

############## MAIN ##############

print("*** Tic Tac Toe ***\n")

players=initPlayers()
grid=createEmptyGrid()

turn=0
endgame=False
input_valid=False

while not endgame:
  drawGrid(grid)

  current_turn=turn%NB_PLAYERS
  player=players[current_turn]

  res=input(f"Your turn {player['nom']} ! Enter position x,y (i.e 1,2) to play or 'q' to quit the game: ")

  if res=='q':
    endgame=True
  elif checkCoord(res) :
    coord=getCoord(res)
    if(isCellOccupied(grid,coord)):
      updateGrid(grid,coord,player["sign"])
      if(checkGrid(grid,coord,player["sign"])):
        drawGrid(grid)
        endgame=True
      turn+=1
    else:
      print(f"Error: The position ({coord[0]},{coord[1]}) is already occupied... Try another position\n")
  else:
    print(f"Error: Invalid input. Try again!\n")

print("\nThank you for playing!")
